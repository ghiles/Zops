# Zops - Zabbix OPS
Un ensemble de playbook ANSIBLE pour l'installation et le maintien d'une plateforme Zabbix.

### Roles :
 - server: pour l'installation des serveurs Zabbix.
 - front: pour l'installation des serveurs Front.
 - BDD: installation des serveurs de Base de données( mariaDB).
 - proxy: installation des proxies.
 - HA: installation des prérequis pour la Haute disponibilité.
 - Common: commun à tout les serveurs (Repository, selinux, zabbix-agent ..etc)
 
## Utilisation
Renseingnez dans fichier Zops/hosts l'inventaire des serveurs Zabbix.

Dans les fichiers defaults/main.yml vous pouvez customiser l'ensemble des variables du roles, 
Exemple: Zops/roles/server/defaults/main.yml 
```
---
# defaults file for server
zabbix_server_logfilesize: 0
zabbix_server_debuglevel: 3
zabbix_server_dbport: 3306
zabbix_server_exportfilesize: 1G
zabbix_server_startpollers: 20
zabbix_server_startipmipollers: 0
zabbix_server_startpreprocessors: 3
zabbix_server_startpollersunreachable: 20
zabbix_server_starttrappers: 10
zabbix_server_startpingers: 10
zabbix_server_startdiscoverers: 1
zabbix_server_starthttppollers: 1
zabbix_server_starttimers: 1
zabbix_server_startescalators: 1
zabbix_server_startalerters: 3
zabbix_server_javagateway:
zabbix_server_javagatewayport: 10052
zabbix_server_startjavapollers: 0
zabbix_server_startvmwarecollectors: 0
zabbix_server_vmwarefrequency: 60
zabbix_server_vmwareperffrequency: 60
zabbix_server_vmwarecachesize: 8M
zabbix_server_vmwaretimeout: 10
zabbix_server_startsnmptrapper: 1
zabbix_server_listenip: 0.0.0.0
zabbix_server_housekeepingfrequency: 1
zabbix_server_maxhousekeeperdelete: 5000
# zabbix_server_senderfrequency: 30
zabbix_server_cachesize: 32M
zabbix_server_cacheupdatefrequency: 60
zabbix_server_startdbsyncers: 4
zabbix_server_historycachesize: 256M
zabbix_server_historyindexcachesize: 32M
zabbix_server_trendcachesize: 64M
# zabbix_server_historytextcachesize: 16M
zabbix_server_valuecachesize: 64M
#zabbix_server_nodenoevents: 0
#zabbix_server_nodenohistory: 0
zabbix_server_timeout: 4
zabbix_server_trappertimeout: 300
zabbix_server_unreachableperiod: 45
zabbix_server_unavailabledelay: 60
zabbix_server_unreachabledelay: 15
zabbix_server_sshkeylocation:
zabbix_server_logslowqueries: 3000
zabbix_server_tmpdir: /tmp
zabbix_server_startproxypollers: 1
zabbix_server_proxyconfigfrequency: 3600
zabbix_server_proxydatafrequency: 1
zabbix_server_allowroot: 0
zabbix_server_statsallowedip:

```

Pour lancer le playbook: 
```
ansible-playbook -i Zops/hosts Zops/site.yml 
```
